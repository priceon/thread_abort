using System;
using System.Configuration;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;

[assembly: WebActivator.PreApplicationStartMethod(typeof(WebApplication1.App_Start.AppHost), "Start")]


/**
 * Entire ServiceStack Starter Template configured with a 'Hello' Web Service and a 'Todo' Rest Service.
 *
 * Auto-Generated Metadata API page at: /metadata
 * See other complete web service examples at: https://github.com/ServiceStack/ServiceStack.Examples
 */

namespace WebApplication1.App_Start
{
	public class AppHost : AppHostBase
	{		
		public AppHost() //Tell ServiceStack the name and where to find your web services
			: base("StarterTemplate ASP.NET Host", typeof(HelloService).Assembly) { }

		public override void Configure(Funq.Container container)
		{
			//Set JSON web services to return idiomatic JSON camelCase properties
			ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;
		
			//Configure User Defined REST Paths
			Routes
				.Add<Hello>("/test");

			//Uncomment to change the default ServiceStack configuration
            //SetConfig(new HostConfig {
            //});

			//Enable Authentication
			//ConfigureAuth(container);

			//Register all your dependencies	
		}

		/* Example ServiceStack Authentication and CustomUserSession */
		private void ConfigureAuth(Funq.Container container)
		{
		}

		public static void Start()
		{
			new AppHost().Init();
		}

		public override void Release(object instance)
		{
			if (instance.GetType().ToString().StartsWith("NewRelic.Agent", StringComparison.CurrentCultureIgnoreCase))
				return;

			base.Release(instance);
		}
	}
}
