using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using ServiceStack;

namespace WebApplication1
{
    //Request DTO
    public class Hello
    {
    }

    //Response DTO
    public class HelloResponse
    {
        public string Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
    }

    //Can be called via any endpoint or format, see: http://mono.servicestack.net/ServiceStack.Hello/
    public class HelloService : Service
    {
        public object Any(Hello request)
        {
			Thread.Sleep(TimeSpan.FromMinutes(5));
	        return "test";
        }
    }

}

